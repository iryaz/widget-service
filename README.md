## widget-service

The app runs in an embedded tomcat server listening on port 8080
    -  http://localhost:8080/api/widget/    
     
### Widget model: 
       {
            "id": "c0f12852-8d64-4c4d-9760-83d32952b6ec",
            "x": 50,
            "y": 50,
            "z": 200,
            "width": 100,
            "height": 100,
            "modificationDate": "2021-02-07T23:25:16.951"
        }

### Requirements:
- Language - Java 11; Framework - Spring Boot; Build tool - Maven;
- Do only the server-side, you don’t need to do visualization;
- No need to do any authorization;
- No explicit limits on memory or runtime, but the more efficient the implementation, the better;
- You can assume that 90% of load distribution is read operations;
- The API should respect to REST architecture;
- Data should be stored in memory. You can use any classes of the standard library to
organize storage. Using any external repositories and databases is forbidden.
- All changes to widgets must occur atomically. The Z-index must be unique.
- At least 30% of the code should be covered by unit and integration tests;
- Submit sources via a public git repository.

### Complications:
1. Pagination done.
2. Filtering done(only for "in-memory" profile, author didn't have time =) ).
3. SQL database done(H2). App includes 2 profiles: 
    - in-memory - data should be stored in memory.
    - h2database - data should be stored in H2 database.
    
    
### Test coverage:
![Test Coverage](coverage.PNG)

### Application build:
    mvn clean install
    
### Application run:   
    mvn spring-boot:run -Dspring-boot.run.profiles=profile
profile - in-memory or h2database     


## Application api :

    - POST /api/widget  
    Creating a new widget, passing a widget as json in body request.
    Example:
        {
            "x": 0,
            "y": 50,
            "z": 1,
            "width": 100,
            "height": 100
        }
        
    - PUT /api/widget 
    Updating the widget's attributes by id. Z-index will be updated on Details point.
    Example:
            {
                "id": "f1f2bf77-a831-4b05-b2fd-576854697fce",
                "x": 500,
                "y": 500,
                "z": 2,
                "width": 100,
                "height": 100
            }
                
    - DELETE /api/widget/{id} 
    Deleting widget by its id. 
    id - type 4 (pseudo randomly generated) UUID.
    
    - GET /api/widget/{id}
    Retrieving a Widget by its identifier.
    id - type 4 (pseudo randomly generated) UUID.
    
    - GET /api/widget/all
    Retrieving all widgets ordering them by Z-Index ascending with pagination.
    Request parameters: page - page number , pageSize - number of words per page(default: 10, max: 500).
    Example: /api/widget/all?page=1&pageSize=10
    
    
    - GET /api/widget/filter/{fromX}/{toX}/{fromY}/{toY}
    Retrieving the list of widgets in the area.
       


   
 
