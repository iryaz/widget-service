package com.test.miro.widget.cache;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface WidgetCache {

    Widget save(Widget object) throws BadWidgetRequestException;

    boolean delete(UUID id);

    Optional<Widget> findById(UUID id);

    Collection<Widget> getPage(int page, int pageSize);

    Collection<Widget> filter(Integer fromX, Integer toX, Integer fromY, Integer toY);

    void clear();
}
