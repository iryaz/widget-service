package com.test.miro.widget.cache;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Slf4j
@Component
public final class InMemoryWidgetCache implements WidgetCache {

    private final int Z_INCREMENT = 1;
    private final Lock readLock;
    private final Lock writeLock;

    /**
     * Map to store the Widgets [WidgetId, Widget]
     */
    private final Map<UUID, Widget> widgetStorageById = new HashMap();

    /**
     * A map [Z-Index, WidgetId]
     */
    private final NavigableMap<Integer, UUID> widgetStorageByZ = new TreeMap<>();

    private final NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> widgetsSortedByXYMinMap = new TreeMap();
    private final NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> widgetsSortedByXYMaxMap = new TreeMap();

    public InMemoryWidgetCache() {
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        readLock = readWriteLock.readLock();
        writeLock = readWriteLock.writeLock();
    }

    private Integer getMaxZ() {
        if (widgetStorageByZ.isEmpty()) {
            return 0;
        } else {
            return widgetStorageByZ.lastKey() + Z_INCREMENT;
        }
    }

    private Widget findWidgetById(UUID id) {
        return widgetStorageById.get(id);
    }

    private void insertWidget(final Widget insertWidget, final boolean shift) {
        if (shift) {
            Optional.ofNullable(insertWidget)
                    .map(newWidget -> widgetStorageById.get(newWidget.getId()))
                    .ifPresent(oldWidget -> {
                        widgetStorageByZ.remove(oldWidget.getZ());
                    });
            List<Integer> reverseZKeys = new ArrayList<>(widgetStorageByZ
                    .navigableKeySet()
                    .subSet(insertWidget.getZ(), true, widgetStorageByZ.lastKey(), true));

            Widget swapWidget = insertWidget;
            for (Integer z : reverseZKeys) {
                if (z.equals(swapWidget.getZ())) {
                    Integer newZIndexForOldWidget = z + Z_INCREMENT;
                    Widget oldWidget = widgetStorageById.get(widgetStorageByZ.get(z));
                    oldWidget.setZ(newZIndexForOldWidget);
                    widgetStorageByZ.put(z, swapWidget.getId());
                    swapWidget = oldWidget;
                } else {
                    widgetStorageByZ.put(swapWidget.getZ(), swapWidget.getId());
                    break;
                }
            }
            if (widgetStorageByZ.lastKey() < swapWidget.getZ()) {
                widgetStorageByZ.put(swapWidget.getZ(), swapWidget.getId());
            }
        }
        widgetStorageById.put(insertWidget.getId(), insertWidget);
        widgetStorageByZ.put(insertWidget.getZ(), insertWidget.getId());
        putWidgetInXYMap(insertWidget);
    }

    private void resolveNewWidgetByOld(final Widget widget, Widget oldWidget) {
        log.debug("resolve old - {} and new - {}", oldWidget, widget);
        if (widget.getZ() == null)
            widget.setZ(oldWidget.getZ());
        if (widget.getX() == null)
            widget.setX(oldWidget.getX());
        if (widget.getY() == null)
            widget.setY(oldWidget.getY());
        if (widget.getHeight() == null)
            widget.setHeight(oldWidget.getHeight());
        if (widget.getWidth() == null)
            widget.setWidth(oldWidget.getWidth());
    }

    private void removeWidget(UUID id) {
        Widget removedWidget = widgetStorageById.remove(id);
        removeWidgetFromXYMaps(removedWidget);
        widgetStorageByZ.remove(removedWidget.getZ());
    }

    @Override
    public Widget save(Widget widget) throws BadWidgetRequestException {
        writeLock.lock();
        try {
            widget.setModificationDate(LocalDateTime.now());
            if (widget.getId() == null) {
                widget.setId(UUID.randomUUID());//new
            } else {
                Widget oldWidget = findById(widget.getId())
                        .orElseThrow(() -> new BadWidgetRequestException("{\"error\":\"Widget can't be null\"}"));
                removeWidgetFromXYMaps(oldWidget);
                resolveNewWidgetByOld(widget, oldWidget);
            }

            if (widget.getZ() == null) {
                widget.setZ(getMaxZ());
            }
            //Check by Z-index
            UUID widgetIdByZ = widgetStorageByZ.get(widget.getZ());
            insertWidget(widget, widgetIdByZ != null && !widgetIdByZ.equals(widget.getId()));
            return widget;
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public Optional<Widget> findById(UUID id) {
        readLock.lock();
        try {
            return Optional.ofNullable(findWidgetById(id));
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public Collection<Widget> getPage(int page, int pageSize) {
        var startIndex = (page - 1) * pageSize;
        if (startIndex >= widgetStorageByZ.size()) {
            return Collections.EMPTY_LIST;
        }

        readLock.lock();
        try {
            return widgetStorageByZ
                    .values()
                    .stream()
                    .skip(startIndex)
                    .limit(pageSize)
                    .map(widgetStorageById::get)
                    .collect(Collectors.toList());
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public boolean delete(UUID id) {
        writeLock.lock();
        try {
            if (widgetStorageById.containsKey(id)) {
                removeWidget(id);
                return true;
            } else {
                return false;
            }
        } finally {
            writeLock.unlock();
        }
    }

    private void putWidgetInXYMap(Widget widget) {
        if (widget.getX() != null && widget.getY() != null && widget.getWidth() != null && widget.getHeight() != null) {
            Integer xMin = widget.getX();
            Integer yMin = widget.getY();
            Integer xMax = xMin + widget.getWidth();
            Integer yMax = yMin + widget.getHeight();
            putInXYMap(xMin, yMin, widget, widgetsSortedByXYMinMap);
            putInXYMap(xMax, yMax, widget, widgetsSortedByXYMaxMap);
        }
    }

    private void putInXYMap(Integer x, Integer y, Widget widget, NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> map) {
        NavigableMap<Integer, Set<Widget>> yMap = Optional.ofNullable(map.get(x)).orElse(new TreeMap());
        Set<Widget> widgets = Optional.ofNullable(yMap.get(y)).orElse(new HashSet<>());
        widgets.add(widget);
        yMap.put(y, widgets);
        map.put(x, yMap);
    }

    private void removeWidgetFromXYMaps(Widget widget) {
        removeWidgetFromXYMap(widget, widgetsSortedByXYMinMap);//remove (Xmin, Ymin) -> widget
        removeWidgetFromXYMap(widget, widgetsSortedByXYMaxMap);//remove (Xmax, Ymax) -> widget
    }

    private void removeWidgetFromXYMap(Widget widget, NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> map) {
        Optional.ofNullable(map.get(widget.getX())).ifPresent(
                yMap -> {
                    Optional.ofNullable(yMap.get(widget.getY())).ifPresent(
                            widgets -> {
                                widgets.remove(widget);
                                if (widgets.isEmpty()) {
                                    yMap.remove(widget.getY());
                                }
                            });
                    if (yMap.isEmpty()) {
                        map.remove(widget.getX());
                    }
                }
        );
    }

    private Set<Widget> getWidgetsInRange(Integer fromX, Integer toX, Integer fromY, Integer toY,
                                          NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> map, boolean includeTail, boolean includeHead) {
        Set<Widget> widgets = new HashSet<>();
        NavigableMap<Integer, NavigableMap<Integer, Set<Widget>>> catMapByXRange = map.tailMap(fromX, includeTail).headMap(toX, includeHead);
        for (NavigableMap<Integer, Set<Widget>> mapByY : catMapByXRange.values()) {
            mapByY.tailMap(fromY, includeTail)
                    .headMap(toY, includeHead)
                    .values().forEach(set -> widgets.addAll(set));
        }
        return widgets;
    }

    @Override
    public Collection<Widget> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) {
        readLock.lock();
        try {
            Set<Widget> widgetsByMinXY = getWidgetsInRange(fromX, toX, fromY, toY, widgetsSortedByXYMinMap, true, false);// points - (Xmin, Ymin)
            Set<Widget> widgetsByMaxXY = getWidgetsInRange(fromX, toX, fromY, toY, widgetsSortedByXYMaxMap, false, true);// points - (Xmax, Ymax)
            widgetsByMaxXY.retainAll(widgetsByMinXY); // intersection -> widgetsByMaxXY
            return widgetsByMaxXY;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public void clear() {
        widgetStorageById.clear();
        widgetStorageByZ.clear();
        widgetsSortedByXYMinMap.clear();
        widgetsSortedByXYMaxMap.clear();
    }
}
