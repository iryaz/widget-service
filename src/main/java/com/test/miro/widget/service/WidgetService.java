package com.test.miro.widget.service;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.dto.WidgetDto;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface WidgetService {

    WidgetDto createWidget(WidgetDto widget) throws BadWidgetRequestException;

    WidgetDto updateWidget(WidgetDto widget) throws BadWidgetRequestException;

    boolean deleteWidget(UUID id) throws BadWidgetRequestException;

    Optional<WidgetDto> findWidgetById(UUID id) throws BadWidgetRequestException;

    Collection<WidgetDto> getPage(Optional<Integer> page, Optional<Integer> pageSize);

    Collection<WidgetDto> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) throws BadWidgetRequestException;
}
