package com.test.miro.widget.service.impl;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.dto.WidgetDto;
import com.test.miro.widget.model.entity.Widget;
import com.test.miro.widget.repository.WidgetRepository;
import com.test.miro.widget.service.WidgetService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WidgetServiceImpl implements WidgetService {

    private static final int MIN_PAGE_NUMBER = 1;

    @Value("${application.pageSize.default}")
    private int pageSizeDefault;

    @Value("${application.pageSize.max}")
    private int pageSizeMax;

    private WidgetRepository widgetRepository;

    private ModelMapper modelMapper;

    @Autowired
    public WidgetServiceImpl(WidgetRepository widgetRepository, ModelMapper modelMapper) {
        this.widgetRepository = widgetRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public WidgetDto createWidget(WidgetDto widgetDto) throws BadWidgetRequestException {
        checkAttributes(widgetDto, true);
        widgetDto.setId(null);
        log.debug("Creating a widget {}", widgetDto);
        var widgetSaved = widgetRepository.save(modelMapper.map(widgetDto, Widget.class));
        return modelMapper.map(widgetSaved, WidgetDto.class);
    }

    @Override
    public WidgetDto updateWidget(WidgetDto widgetDto) throws BadWidgetRequestException {
        checkAttributes(widgetDto, false);
        log.debug("Updating a widget {}", widgetDto);
        var widgetUpdated = widgetRepository.save(modelMapper.map(widgetDto, Widget.class));
        return modelMapper.map(widgetUpdated, WidgetDto.class);
    }

    @Override
    public boolean deleteWidget(UUID id) throws BadWidgetRequestException {
        if (id == null) {
            throw new BadWidgetRequestException("{\"error\":\"Widget ID can't be null\"}");
        }
        log.debug("Removing a widget {}", id);
        return widgetRepository.deleteById(id);
    }

    @Override
    public Optional<WidgetDto> findWidgetById(UUID id) throws BadWidgetRequestException {
        if (id == null) {
            throw new BadWidgetRequestException("{\"error\":\"Widget ID can't be null\"}");
        }
        log.debug("Find widget by id {}", id);
        Optional<Widget> widget = widgetRepository.findById(id);
        return widget.map(w -> modelMapper.map(w, WidgetDto.class));
    }

    @Override
    public Collection<WidgetDto> getPage(Optional<Integer> page, Optional<Integer> pageSize) {
        log.debug("Find all Widgets");
        Collection<Widget> widgetList = widgetRepository.getPage(Math.max(MIN_PAGE_NUMBER, page.orElse(MIN_PAGE_NUMBER)),
                Math.min(pageSize.orElse(pageSizeDefault), pageSizeMax));

        return widgetList.stream().map(w -> modelMapper.map(w, WidgetDto.class)).collect(Collectors.toList());
    }

    @Override
    public Collection<WidgetDto> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) throws BadWidgetRequestException {
        checkFilterRange(fromX, toX, fromY, toY);
        Collection<Widget> widgetFilteredList = widgetRepository.filter(fromX, toX, fromY, toY);
        return widgetFilteredList.stream().map(w -> modelMapper.map(w, WidgetDto.class)).collect(Collectors.toList());
    }

    private void checkAttributes(WidgetDto widget, boolean isNew) throws BadWidgetRequestException {
        if (widget == null) {
            throw new BadWidgetRequestException("{\"error\":\"Widget can't be null\"}");
        }
        if (widget.getHeight() == null || widget.getWidth() == null
                || widget.getHeight() <= 0 || widget.getWidth() <= 0) {
            throw new BadWidgetRequestException("{\"error\":\"Height and Width attributes can't be empty and less than or equal to zero\"}");
        }
        if (widget.getX() == null || widget.getY() == null) {
            throw new BadWidgetRequestException("{\"error\":\"X, Y attributes can't be empty\"}");
        }
        if (!isNew && widget.getId() == null) {
            throw new BadWidgetRequestException("{\"error\":\"Id can't be empty for updatable widget\"}");
        }
    }

    private void checkFilterRange(Integer fromX, Integer toX, Integer fromY, Integer toY) throws BadWidgetRequestException {
        if (fromX == null || toX == null || fromY == null || toY == null ||
                fromX > toY || fromY > toY) {
            throw new BadWidgetRequestException("{\"error\":\"Filter range was set incorrectly\"}");
        }
    }


}
