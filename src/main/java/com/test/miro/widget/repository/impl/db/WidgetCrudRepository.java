package com.test.miro.widget.repository.impl.db;

import com.test.miro.widget.model.entity.Widget;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@Profile("h2database")
public interface WidgetCrudRepository extends CrudRepository<Widget, UUID> {

    List<Widget> findAllByZGreaterThanEqualOrderByZAsc(Integer zIndex);

    List<Widget> findAllByOrderByZ(Pageable pageable);

    Optional<Widget> findTopByOrderByZ();
}
