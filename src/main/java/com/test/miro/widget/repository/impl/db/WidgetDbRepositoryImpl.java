package com.test.miro.widget.repository.impl.db;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;
import com.test.miro.widget.repository.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
@Profile("h2database")
public class WidgetDbRepositoryImpl implements WidgetRepository {

    private final int Z_INCREMENT = 1;

    private final WidgetCrudRepository repository;

    @Autowired
    public WidgetDbRepositoryImpl(WidgetCrudRepository repository) {
        this.repository = repository;
    }

    @Transactional
    @Override
    public Widget save(Widget widget) {
        if (widget.getZ() == null) {
            widget.setZ(getTopZ());
        } else {
            shiftUpZ(widget.getZ());
        }
        return repository.save(widget);
    }

    @Override
    public Optional<Widget> findById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public List<Widget> getPage(int page, int pageSize) {
        return repository.findAllByOrderByZ(PageRequest.of(page - 1, pageSize));
    }

    @Transactional
    @Override
    public boolean deleteById(UUID id) {
        if (findById(id).isEmpty()) {
            return false;
        } else {
            repository.deleteById(id);
            return true;
        }
    }

    @Override
    public List<Widget> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) throws BadWidgetRequestException {
        throw new BadWidgetRequestException("{\"error\":\"Method not supported in h2database profile, author didn't have time =)\"}");
    }

    private void shiftUpZ(int z) {
        Map<Integer, Widget> widgetMap = repository.findAllByZGreaterThanEqualOrderByZAsc(z).stream()
                .collect(Collectors.toMap(Widget::getZ, Function.identity()));
        int shiftedZ = z;
        List<Widget> forUpdateWithNewZ = new ArrayList();
        for (Map.Entry<Integer, Widget> entry : widgetMap.entrySet()) {
            if (entry.getKey().equals(shiftedZ)) {
                Widget widget = entry.getValue();
                shiftedZ += Z_INCREMENT;
                widget.setZ(shiftedZ);
                forUpdateWithNewZ.add(widget);
            } else {
                break;
            }
        }
        repository.saveAll(forUpdateWithNewZ);
    }

    private Integer getTopZ() {
        Optional<Widget> optionalWidget = repository.findTopByOrderByZ();
        if (optionalWidget.isEmpty()) {
            return 0;
        }
        return optionalWidget.get().getZ() + 1;
    }
}
