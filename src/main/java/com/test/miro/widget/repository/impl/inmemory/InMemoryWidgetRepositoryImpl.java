package com.test.miro.widget.repository.impl.inmemory;

import com.test.miro.widget.cache.InMemoryWidgetCache;
import com.test.miro.widget.cache.WidgetCache;
import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;
import com.test.miro.widget.repository.WidgetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Repository
@Profile("in-memory")
public class InMemoryWidgetRepositoryImpl implements WidgetRepository {

    private WidgetCache cache;

    @Autowired
    public InMemoryWidgetRepositoryImpl(InMemoryWidgetCache cache) {
        this.cache = cache;
    }

    @Override
    public Widget save(Widget widget) throws BadWidgetRequestException {
        return cache.save(widget);
    }

    @Override
    public Optional<Widget> findById(UUID id) {
        return cache.findById(id);
    }

    @Override
    public Collection<Widget> getPage(int page, int pageSize) {
        return cache.getPage(page, pageSize);
    }

    @Override
    public boolean deleteById(UUID id) {
        return cache.delete(id);
    }

    @Override
    public Collection<Widget> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) {
        return cache.filter(fromX, toX, fromY, toY);
    }
}
