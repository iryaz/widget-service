package com.test.miro.widget.repository;

import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface WidgetRepository {

    Widget save(Widget widget) throws BadWidgetRequestException;

    Optional<Widget> findById(UUID id);

    Collection<Widget> getPage(int page, int pageSize);

    boolean deleteById(UUID id);

    Collection<Widget> filter(Integer fromX, Integer toX, Integer fromY, Integer toY) throws BadWidgetRequestException;
}
