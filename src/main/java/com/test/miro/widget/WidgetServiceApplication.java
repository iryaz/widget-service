package com.test.miro.widget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class WidgetServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WidgetServiceApplication.class, args);
	}

}
