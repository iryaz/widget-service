package com.test.miro.widget.exception;

public class BadWidgetRequestException extends Exception {

    private static final long serialVersionUID = 7402415353037260889L;

    public BadWidgetRequestException(String message) {
        super(message);
    }
}
