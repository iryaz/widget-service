package com.test.miro.widget.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WidgetDto implements Serializable {

    private static final long serialVersionUID = -3476486178689925560L;

    private UUID id;
    private Integer x;
    private Integer y;
    private Integer z;
    private Integer width;
    private Integer height;
    private LocalDateTime modificationDate;
}
