package com.test.miro.widget.controller;


import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.dto.WidgetDto;
import com.test.miro.widget.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping(value = "/api/widget")
public class WidgetController {

    private WidgetService widgetService;

    @Autowired
    public WidgetController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    @PostMapping
    public ResponseEntity<WidgetDto> create(@RequestBody WidgetDto widget) throws BadWidgetRequestException {
        return new ResponseEntity<>(widgetService.createWidget(widget), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody WidgetDto widget) throws BadWidgetRequestException {
        return new ResponseEntity<>(widgetService.updateWidget(widget), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") UUID id) throws BadWidgetRequestException {
        if(widgetService.deleteWidget(id)){
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<WidgetDto> findById(@PathVariable("id") UUID id) throws BadWidgetRequestException {
        return widgetService.findWidgetById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/all")
    public Collection<WidgetDto> getPage(@RequestParam(value = "page") Optional<Integer> page,
                                @RequestParam(value = "pageSize") Optional<Integer> pageSize) {
        return widgetService.getPage(page, pageSize);
    }

    @GetMapping("/filter/{fromX}/{toX}/{fromY}/{toY}")
    public Collection<WidgetDto> filter(@PathVariable("fromX") Integer fromX,
                                        @PathVariable("fromY") Integer fromY,
                                        @PathVariable("toX") Integer toX,
                                        @PathVariable("toY") Integer toY) throws BadWidgetRequestException {
        return widgetService.filter(fromX, toX, fromY, toY);
    }

    @ExceptionHandler(BadWidgetRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final String exceptionHandlerBadWidgetException(final BadWidgetRequestException ex) {
        return ex.getMessage();
    }
}
