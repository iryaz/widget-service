package com.test.miro.widget.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.miro.widget.WidgetTestHelper;
import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.dto.WidgetDto;
import com.test.miro.widget.service.WidgetService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@WebMvcTest(controllers = WidgetController.class)
@AutoConfigureMockMvc
class WidgetControllerTest {

    private static final String MAIN_URL = "/api/widget";
    private static final String FILTER_PATH = "/filter/%d/%d/%d/%d";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private WidgetService service;

    @Test
    void checkCreatingWidget() throws Exception {
        var dto = WidgetTestHelper.createTestWidgetDto();
        var predictionDto = WidgetTestHelper.createTestFullWidgetDto();
        Mockito.when(service.createWidget(dto)).thenReturn(predictionDto);
        String response = mockMvc.perform(post(MAIN_URL).content(objectMapper.writeValueAsBytes(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        var widgetCreated = objectMapper.readValue(response, WidgetDto.class);
        Assertions.assertEquals(predictionDto.getZ(), widgetCreated.getZ(), "Different z indexes");
    }

    @Test
    void checkUpdateWidget() throws Exception {
        var dto = WidgetTestHelper.createTestFullWidgetDto();
        var predictionDto = WidgetTestHelper.createTestFullWidgetDto();
        predictionDto.setModificationDate(dto.getModificationDate().minusYears(1));//obviously different
        Mockito.when(service.updateWidget(dto)).thenReturn(predictionDto);
        String response = mockMvc.perform(put(MAIN_URL).content(objectMapper.writeValueAsBytes(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        var widgetCreated = objectMapper.readValue(response, WidgetDto.class);
        Assertions.assertEquals(predictionDto.getZ(), widgetCreated.getZ(), "Different z indexes");
        Assertions.assertEquals(predictionDto.getId(), widgetCreated.getId(), "Different ids");
        Assertions.assertNotEquals(dto.getModificationDate(), widgetCreated.getModificationDate(),
                "modification date wasn't changed");
    }

    @Test
    void checkCreatingWidgetWithZeroWidthAndHeight() throws Exception {
        var testWidgetDto = WidgetTestHelper.createTestWidgetDto();
        testWidgetDto.setWidth(0);
        testWidgetDto.setHeight(null);
        Mockito.when(service.createWidget(testWidgetDto))
                .thenThrow(new BadWidgetRequestException("{\"error\":\"Height and Width attributes can't be empty and less than or equal to zero\"}"));

        mockMvc.perform(post(MAIN_URL))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
    }

    @Test
    void checkSuccessDeleteWidget() throws Exception {
        Mockito.doReturn(true).when(service).deleteWidget(Mockito.any());

        mockMvc.perform(delete(MAIN_URL + "/" + UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    @Test
    void checkFailureDeleteWidget() throws Exception {
        Mockito.doReturn(false).when(service).deleteWidget(Mockito.any());

        mockMvc.perform(delete(MAIN_URL + "/" + UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    @Test
    void checkSuccessFindById() throws Exception {
        var dto = WidgetTestHelper.createTestWidgetDto();
        var id = UUID.randomUUID();
        Mockito.doReturn(Optional.of(dto)).when(service).findWidgetById(id);

        String contentAsString = mockMvc.perform(get(MAIN_URL + "/" + id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse().getContentAsString();
        var result = objectMapper.readValue(contentAsString, WidgetDto.class);
        Assertions.assertEquals(dto.getId(), result.getId());
        Assertions.assertEquals(dto.getZ(), result.getZ());
    }

    @Test
    void checkFailureFilter() throws Exception {
        Mockito.when(service.filter(2, 1, 15, 19))
                .thenThrow(new BadWidgetRequestException("{\"error\":\"Filter range was set incorrectly\"}"));

        mockMvc.perform(get(MAIN_URL + String.format(FILTER_PATH, 2, 1, 15, 19))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }
}