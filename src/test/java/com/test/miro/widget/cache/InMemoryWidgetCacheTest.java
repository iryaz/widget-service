package com.test.miro.widget.cache;

import com.test.miro.widget.WidgetTestHelper;
import com.test.miro.widget.exception.BadWidgetRequestException;
import com.test.miro.widget.model.entity.Widget;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@ExtendWith(SpringExtension.class)
class InMemoryWidgetCacheTest {

    private WidgetCache cache = new InMemoryWidgetCache();

    @BeforeEach
    public void clearCache() {
        cache.clear();
    }

    @Test
    public void checkSaveNewWidget() throws BadWidgetRequestException {
        var savedWidget = saveNewWidget();
        Assertions.assertNotNull(savedWidget.getId(), "Id is null");
    }

    private Widget saveNewWidget() throws BadWidgetRequestException {
        var widget = WidgetTestHelper.createTestWidget();
        return cache.save(widget);
    }

    @Test
    public void checkUpdateWidget() throws BadWidgetRequestException {
        var savedWidget = saveNewWidget();
        var result = cache.findById(savedWidget.getId()).get();
        Assertions.assertEquals(savedWidget, result, "Widgets are different");
    }

    @Test
    public void checkFindWidget() throws BadWidgetRequestException {
        var widgetForUpdate = saveNewWidget();
        widgetForUpdate.setHeight(100);
        var result = cache.save(widgetForUpdate);
        Assertions.assertEquals(widgetForUpdate.getId(), result.getId(), "Ids are different");
        Assertions.assertEquals(widgetForUpdate.getHeight(), result.getHeight(), "Heights are equal");
    }

    @Test
    public void checkDeleteWidget() throws BadWidgetRequestException {
        var savedWidget = saveNewWidget();
        var result = cache.delete(savedWidget.getId());
        Assertions.assertTrue(result, "Widget wasn't deleted");
    }

    @Test
    public void checkGetPage() throws BadWidgetRequestException {
        for (int i = 0; i < 11; i++) {
            saveNewWidget();
        }
        var resultFirstPage = cache.getPage(1, 10);
        var resultSecondPage = cache.getPage(2, 10);
        Assertions.assertEquals(10, resultFirstPage.size(), "Page size not equals expected size");
        Assertions.assertEquals(1, resultSecondPage.size(), "Page size not equals expected size");
    }

    @Test
    public void checkFilter() throws BadWidgetRequestException {
        var firstWidgetInRange = saveNewWidgetWithParams(0, 0, 1, 100, 100);
        var secondWidgetInRange = saveNewWidgetWithParams(0, 50, 1, 100, 100);
        var widgetOutOfRange = saveNewWidgetWithParams(50, 50, 1, 100, 100);
        Collection<Widget> filtered = cache.filter(0, 100, 0, 150);
        Assertions.assertTrue(filtered.contains(firstWidgetInRange), "firstWidgetInRange out of range");
        Assertions.assertTrue(filtered.contains(secondWidgetInRange), "secondWidgetInRange out of range");
        Assertions.assertFalse(filtered.contains(widgetOutOfRange), "widgetOutOfRange in range");
    }

    private Widget saveNewWidgetWithParams(Integer x, Integer y, Integer z, Integer height, Integer width) throws BadWidgetRequestException {
        var widget = new Widget();
        widget.setX(x);
        widget.setY(y);
        widget.setZ(z);
        widget.setHeight(height);
        widget.setWidth(width);
        return cache.save(widget);
    }

    @Test
    public void checkInsertExistZAndShiftGreaterThanOrEqual() throws BadWidgetRequestException {
        saveNewWidgetWithParams(1, 1, 1, 100, 100);
        var widgetIdForShift1 = saveNewWidgetWithParams(2, 2, 2, 100, 100).getId();
        var widgetIdForShift2 = saveNewWidgetWithParams(3, 3, 3, 100, 100).getId();
        var widgetIdForInsert = saveNewWidgetWithParams(4, 4, 2, 100, 100).getId();
        Map<UUID, Widget> widgets = cache.getPage(1, 10).stream().collect(Collectors.toMap(Widget::getId, Function.identity()));
        Assertions.assertEquals(2, widgets.get(widgetIdForInsert).getZ(), "");
        Assertions.assertEquals(3, widgets.get(widgetIdForShift1).getZ(), "");
        Assertions.assertEquals(4, widgets.get(widgetIdForShift2).getZ(), "");
    }

    @Test
    public void checkInsertNewZ() throws BadWidgetRequestException {
        saveNewWidgetWithParams(1, 1, 1, 100, 100);
        var widgetIdForShift1 = saveNewWidgetWithParams(2, 2, 5, 100, 100).getId();
        var widgetIdForShift2 = saveNewWidgetWithParams(3, 3, 6, 100, 100).getId();
        var widgetIdForInsert = saveNewWidgetWithParams(4, 4, 2, 100, 100).getId();
        Map<UUID, Widget> widgets = cache.getPage(1, 10).stream().collect(Collectors.toMap(Widget::getId, Function.identity()));
        Assertions.assertEquals(2, widgets.get(widgetIdForInsert).getZ(), "");
        Assertions.assertEquals(5, widgets.get(widgetIdForShift1).getZ(), "");
        Assertions.assertEquals(6, widgets.get(widgetIdForShift2).getZ(), "");
    }

    @Test
    public void checkInsertExistZAndShiftOneZ() throws BadWidgetRequestException {
        saveNewWidgetWithParams(1, 1, 1, 100, 100);
        var widgetIdForShift1 = saveNewWidgetWithParams(2, 2, 2, 100, 100).getId();
        var widgetIdForShift2 = saveNewWidgetWithParams(3, 3, 4, 100, 100).getId();
        var widgetIdForInsert = saveNewWidgetWithParams(4, 4, 2, 100, 100).getId();
        Map<UUID, Widget> widgets = cache.getPage(1, 10).stream().collect(Collectors.toMap(Widget::getId, Function.identity()));
        Assertions.assertEquals(2, widgets.get(widgetIdForInsert).getZ(), "");
        Assertions.assertEquals(3, widgets.get(widgetIdForShift1).getZ(), "");
        Assertions.assertEquals(4, widgets.get(widgetIdForShift2).getZ(), "");
    }
}