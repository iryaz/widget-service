package com.test.miro.widget;

import com.test.miro.widget.model.dto.WidgetDto;
import com.test.miro.widget.model.entity.Widget;

import java.time.LocalDateTime;
import java.util.UUID;

public class WidgetTestHelper {

    public static WidgetDto createTestFullWidgetDto() {
        return WidgetDto.builder().x(0).y(0).z(1)
                .width(10).height(10)
                .id(UUID.randomUUID()).modificationDate(LocalDateTime.now()).build();
    }

    public static WidgetDto createTestWidgetDto() {
        return WidgetDto.builder().x(0).y(0).width(10).height(10).build();
    }

    public static Widget createTestFullWidget() {
        return Widget.builder().x(0).y(0).z(1)
                .width(10).height(10)
                .id(UUID.randomUUID()).modificationDate(LocalDateTime.now()).build();
    }

    public static Widget createTestWidget() {
        return Widget.builder().x(0).y(0).width(10).height(10).build();
    }
}
